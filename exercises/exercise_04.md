## Exercise 4: New Model Tests

Once you have completely mastered the previous tests, go ahead and implement the following model in your application. Try and come up with all of the appropriate tests for this new model after you make your migrations. 

```python
class User(models.Model):
    first_name = models.CharField(max_length = 40)
    last_name = models.CharField(max_length = 40)
    email = models.CharField(max_length = 60)
    password = models.CharField(max_length = 60)
    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)
```

Follow the patterns from earlier, and test the following things:

    - All of your urls are working
    - All CRUD operations when working directly with the models
    - All CRUD operations when working with the views