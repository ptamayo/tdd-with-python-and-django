## Exercise 2: True TDD

You will notice the below code snippet contains tests, but no logic in our methods. We are designing around the tests, not the other way around. Create the logic necessary to pass all tests.

Make a new file in the **python_tdd** folder we were working in last lesson and call it exercise_02.py. Copy and paste the below code, and then go ahead and add in the logic for our functions.

Make sure you are testing every time you add some new functionality. The great thing about the unittest module is that normally every test will run. In this system, one function isn't necessarily reliant on another, but in a complex system it would be very important that we test to make sure all of the old code works even with new code implemented.

##### python_tdd/exercise_02.py
```python
import unittest

# return the product of two given numbers
def multiply(a,b):
    pass

# return a boolean of whether every value in a given list is positive
def all_positive(l):
    pass

# return the average value of a given list
def average(l):
    pass

# add the largest and smallest values in a list and return the sum
def add_min_max(l):
    pass

class MainTest(unittest.TestCase):
    def test_multiply(self):
        self.assertEqual(multiply(5,4), 20)
        self.assertEqual(multiply(0,100), 0)
        self.assertEqual(multiply(-5,5), -25)
        self.assertEqual(multiply(-5,-5), 25)

    def test_all_positive(self):
        self.assertTrue(all_positive([1,2,3,4]))
        self.assertTrue(all_positive([1234]))
        self.assertFalse(all_positive([1,-2,3,4]))
        self.assertTrue(all_positive([]))

    def test_average(self):
        self.assertEqual(average([1,2,3,4,5]), 3)
        self.assertEqual(average([100]), 100)
        self.assertEqual(average([-100,100]), 0)

        # Notice that we can even create an assert to see if a specific error is raised by our function
        self.assertRaises(ZeroDivisionError, average, [])

    def test_add_minmax(self):
        self.assertEqual(add_min_max([1,2,3,4,5]), 6)
        self.assertEqual(add_min_max([0,0,0,0]), 0)
        self.assertEqual(add_min_max([10,5,0,-5]), 5)

        # Does this give us a hint as to how a specific case should be handled by our function?
        self.assertIsNone(average([]))
```

When you are finished, head to Lesson 3 to see how we can apply these concepts to our web applications.
[Continue to Lesson_03](../lessons/lesson_03_django_urls.md)

