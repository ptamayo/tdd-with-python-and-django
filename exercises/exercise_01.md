## Exercise 1: Implementing Unit Tests

For this exercise, you will be provided a set of functions, and it is up to you to design the TestCase class as well as implement the methods to test.

Make a new file in the **python_tdd** folder we were working in last lesson and call it exercise_01.py. Copy and paste the below code, and then go ahead and add in the tests.

##### python_tdd/exercise_01.py
```python
import unittest

def min(l):
    # Should find and return minimum value in a given list
    min = l[0]
    for val in l:
        if val < min:
            min = val
    return min

def sum_list(l):
    # Should return total value of all list items
    total = 0
    for val in l:
        total += val
    return total

def less_than(a):
    # Should return a bool of whether given value is less than 100
    return a < 100

### For this exercise, work within this class. This is something you will come up with on your own soon ###

class MainTest(unittest.TestCase):
    # tests go here!



```

After you are finished, head to Lesson 2 to continue.
[Continue to Lesson_02](../lessons/lesson_02_tdd.md)
