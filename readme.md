# Welcome to TDD in Python and Django

This will be a small course on how we can write tests when we are coding in Python, and even when we move to a framework like Django. This course assumes some basic knowledge of both Python and Django 2. If you don't know any Django, Lessons 0 - 2 will still be accessible and fun, as they are just Python alone. However if you do know Django, feel free to move onto the rest of the lessons, as we dive deeper into the philosophy of tes driven development.

Thank you for your time, and I hope you enjoy this course. Get started in the lessons folder with Lesson_00.

[Start with Lesson_00](./lessons/lesson_00_intro.md)