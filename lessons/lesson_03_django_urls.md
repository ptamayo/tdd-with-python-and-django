## TDD in Django

As we have seen, Test Driven Development is a very different approach to creating an application compared to non-TDD. Now it's time to use TDD and create a simple web application using Python with Django.

### Tasks

There are a lot of different things we can test in a large application, but seeing as how this is a CRUD app, let's focus on specifically the CRUD operations. We will cover:

    - Testing our urls and determining if they are all working
    - Testing Creation 
    - Testing Reading all of the albums in the system, and then a single album (different endpoints)
    - Testing Updating an album
    - Testing Deleting an album
    - Testing what context is sent back from our view function, to see if we can properly show data

The beautiful thing about our tests is that we will be able to automatically run one or all of them whenever we desire, so any new code we add to the system will be much more likely to interface well with the rest of the application, and if it doesn't we will know very quickly. This beats having to manually walk through testing every feature in our system.

### Prerequisites

    - Working knowledge of Django

### Working in Django

You will find in this repository a project called `tdd_album_crud`. You can download the source code and should be greeted with a bare-bones Django project that we will be adding features to, specifically CRUD operations on a model called Album.

If you'd rather just create your own Django project and then create tests, that will work as well, but this provided project will save you some time if you just want to dive right in.

### Getting Started

Let's dive into the place we will be writing our tests. When you open the project, head into tdd_album_crud/apps/album_crud/tests.py. This will be where we are writing all of our tests for the time being.

Remember, with TDD in mind we want to define some tests first. You will find a single route in our tdd_album_crud/apps/album_crud/urls.py file that calls the only function in tdd_album_crud/apps/album_crud/views.py to simply render a Hello World page.

Before, when we created our tests, we inherited from unittest.TestCase. This class gave us all of our previous assert methods, and was quite handy. But because Django is a lot more complex than those simple functions we were testing before, we will actually be inheriting from a different class this time, django.test.TestCase. This class has all of the functionality that we used before, and then some. For instance, when we start our tests, Django will actually create a test DB that will store new data in memory, but won't store it in our development DB. This is nice in that we can test our CRUD operations without effecting a real database.

All of our tests will be in `tdd_album_crud/apps/album_crud/tests.py`, and we will be adding logic to `tdd_album_crud/apps/album_crud/urls.py` and `tdd_album_crud/apps/album_crud/views.py`

Continue on to Lesson 4 to learn how we can test our urls in Django.
[Continue to Lesson_04](./lesson_04_urls.md)