# Test Driven Development in Python and Django

## What is Test Driven Development

When we as developers sit down and think about a problem, the typical way we go about solving it is to immediately start thinking of a solution. We work on an algorithm or a portion of an application, and then we run it to see what the effect is or if the code worked. While this can work, often times it leads to problems:

    - What if we have written a large block of code and haven't tested it?
    - What if our new code conflicts with the existing code?
    - Is everything that I just created in the moment actually necessary to solve the problem?

Test Driven Development (TDD) is a methodology designed to alleviate these problems. The main tenant is that rather than immediately jump to writing lines of code and crossing your fingers that it all works, instead we create tests ahead of time, and proceed to write code that will satisfy the requirements of the test. In this way, we essentially consider as many edge cases at the outset of a problem, and then design a more robust solution geared toward working with the edges. 

### Prerequisites

While there are a slew of testing utilities in the wild, Python has a great solution as part of the Python standard library called unittest. This is what we will be using and we won't have to install any additional tech other than your normal Python version.

## TDD vs Non-TDD

In broad strokes, the steps of a TDD development cycle is as follows:

    1. Define a test, ideally with multiple different inputs and outputs
    2. Write just enough code to satisfy the test
    3. Run the new test to see if the code passes
        3a. If test passes, continue
        3b. If test fails, revisit code and change only what is necessary to pass test
    4. Run all exisiting tests to make sure that the previous tests still all pass
    5. Refactor

This differs from a non-TDD way to approach a problem:

    1. Write pseudocode to address the problem
    2. Convert pseudocode into actual code
    3. Test to see if code solves the problem
        3a. If test passes, continue
        3b. If test fails, revisit code and solve issue
    4. Refactor

With both of the methodologies, we can see that testing is typically involved. However, the biggest difference is *when* the test happens. With non-TDD, the test ends up being an afterthought; simply a way to validate that the code we wrote works. With TDD, the entire codebase is designed to conform to pre-existing tests, which are basically a rigorous set of rules that we put in place. When we consider a problem from this angle, we can more easily predict the behavior of potential cases, because we designed the solution around them intentionally.

See you in Lesson 1, where you will see your first test in Python.
[Continue to Lesson_01](./lessons/lesson_01_unittest.md)

