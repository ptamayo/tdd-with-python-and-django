## TDD in Django - URLs

First things first, let's test that single URL and function to see if we can make a request and get a response. We can very easily simulate requests in Django by importing the Client module from django.test. 

After we import the Client, let's get to defining our TestCase. All of our test methods go inside of this class just like before, so we can start off by creating the class and a simple method to make sure the test is being called properly.


##### tdd_album_crud/apps/album_crud/tests.py
```python
from django.test import TestCase, Client

class AlbumTest(TestCase):
    def test_index(self):
        print("Hello")
```

To run the tests, make sure you are in the directory where `manage.py` is present, like when you run your server, and go ahead and type `python manage.py test`. You should see a similar output to what we got before! Welcome to testing in Django. When this is working, we will move onto the next step of implementing a Client, which will allow us to simulate the requests a user will make.

To do this, we will instantiate client in our 'test_index' method, ie: 

`c = Client()`

And after that, let's simulate a request. What we will get back will be a Django Response object, of which we can read more about here: 

https://docs.djangoproject.com/en/2.1/ref/request-response/#id2

For our purposes, we will mainly be looking at the status code, and will be expecting only a few:

    - 200 OK: We have a working url with a valid view function
    - 302 Redirect: We have a working url with a view function that redirects
    - 404 Not Found: We don't have the url in our application

Let's test that the root route of our application, "/", works properly and that we get a 200 code back in our response.

##### tdd_album_crud/apps/album_crud/tests.py
```python
from django.test import TestCase, Client

class AlbumTest(TestCase):
    def test_index(self):
        # print("Hello")

        # adding
        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 200)
```

Run the tests again and we can now see that the test should pass succesfully with the starter code provided. Now we have an easy way to test one or all of our routes. Part of the the next exercise will be coming up will be to create appropriate tests for all of the endpoints in our application.

Following our TDD principles, let's create a test for a route we know doesn't exist, and then we will implement the code in order to make the test pass.

##### tdd_album_crud/apps/album_crud/tests.py
```python
from django.test import TestCase, Client

class AlbumTest(TestCase):

    # Adding below test_index
    def test_create_album(self):
        c = Client()
        response = c.get('/album/create')
        self.assertEqual(response.status_code, 302)
```

Our intention is that at some point we will have a route call the view function in charge of creating the entity in our database, after which it will redirect to "/". Run our tests and we can expect the original `test_index` to pass, and this new test to fail.

After it fails, it's time to code. Add the appropriate url to our urls.py file and a function that just redirects back to the index route. Then test again.

#### Here are the changes to get a pass:

First by adding this path the our urlpatterns:
##### tdd_album_crud/apps/album_crud/urls.py
```python
path('album/create', views.create),
```

And adding this method to our views:
##### tdd_album_crud/apps/album_crud/views.py
```python
def create(request):
    # Simulating post response
    return redirect('/')
```

If we test again, we can see that both test should pass, meaning all of our urls are working. While we didn't use TDD to define the first route in the system, the second route was created by implementing a test, having it fail, and then creating just enough code to get the test to pass.

We are at the point in the cycle where we should consider refactoring, considering our two test methods share most of their code. Let's remove those two and create this method instead:

##### tdd_album_crud/apps/album_crud/tests.py
```python
class AlbumTest(TestCase):
    def test_urls(self):
        c = Client()
        idx_response = c.get('/')
        self.assertEqual(idx_response.status_code, 200)
        create_response = c.get('/album/create')
        self.assertEqual(create_response.status_code, 302)
```

In this way, we test both routes with a single test. We can also keep adding to this function if we want to establish more routes in the system.

In Lesson 5, we will continue on to test our models and make sure everything that already exists is working properly.
[Continue to Lesson_05](./lesson_05_django_models.md)
