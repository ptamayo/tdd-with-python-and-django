## True TDD

Now that you have gotten used to how we can test existing code, it is time that we fully dive into the TDD mindset. Remember the main concepts in a TDD workflow:

    1. Define a test, ideally with multiple different inputs and outputs
    2. Write just enough code to satisfy the test
    3. Run the new test to see if the code passes
        3a. If test passes, continue
        3b. If test fails, revisit code and change only what is necessary to pass test
    4. Run all exisiting tests to make sure that the previous tests still all pass
    5. Refactor

Note, before a single line of code is written to solve a problem, we devise a test. The test is the one running the show, not the solution.

### 1. Define a Test

Consider if we were tasked with creating an algorithm that would scale every value in a list by some factor. Let's imagine the function will be called scale_list. Hypothesize a few test cases in pseudo:

```
scale_list given [1,2,3,4] and 5
returns [5,10,15,20]
```

```
scale_list given [-1,2,-3,4] and -5
returns [5,-10,15,-20]
```

```
scale_list given [] and 0
returns []
```

By no means is this every possible scenario that might happen, but it is a jumping off point. Always consider a few general cases, and we can make the tests more robust later if need be. 

### 2. Write Enough Code to Satisfy

We want to ensure that the code we are adding to some function is only code to solve a single, specific issue. The code will more than likely change again at some point, but by making small incremental changes we can solve one test at a time.

So, in terms of this specific test, it looks like we will have to iterate over the list, multiplying each value by the second argument given. Finally, we will have to return the modified list.

Let's start by defining the TestCase class and our method with the appropriate test inputs we created in pseudocode

```python
import unittest

# All we are doing here is defining the function without any logic. If we didn't do this, the tests would error rather than fail. When we run the test with this function defined we will get a fail instead of an error
def scale_list(l,y):
    pass

class MainTest(unittest.TestCase):
    def test_scale_list(self):
        self.assertEqual(scale_list([1,2,3,4],5), [5,10,15,20])
        self.assertEqual(scale_list([-1,2,-3,4],-5), [5,-10,15,-20])
        self.assertEqual(scale_list([0],0), [])

```

### 3. Run Tests and Modify Code

Let's run our tests now and see the result.

We should have gotten 1 failure: `AssertionError: None != [5, 10, 15, 20]`. While the code doesn't work, our test did. It didn't pass, but now we are at the point in the process where we start to implement a solution. What can we add to the function that will make our test pass?

```python
def scale_list(l,y):
    # What if we returned this instead? Would any of our assert methods pass?
    return [5,10,15,20]
```

Of course we wouldn't just send back a list of hardcoded values, but this brings up an interesting point: we always need multiple asserts with different inputs to determine if the function is working properly. This function will technically satisfy the first assert statement we wrote, the one expecting `[5,10,15,20]`, however it won't work for the others. Be cautious when designing your solution, and try to consider as many edge cases as possible while you are testing.

By actively trying to break your code, you will become a better TDD developer. Let's try again:

```python
def scale_list(l,y):
    for i in range(len(l)):
        l[i] *= y
    return l
```

Run your tests after making these changes and let's see if we get a pass. 

We should now see that the all of our individual assert statements passed, and therefore the test we wrote passes with flying colors. We have succesfully designed a solution by using a test as a blueprint, which is the main concept behind Test Driven Development!

Now, head to Exercise 2 to solidify the TDD mindset!
[Try Exercise_02](../exercises/exercise_02.md)


