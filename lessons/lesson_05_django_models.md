## TDD in Django - Models

When it comes to models, there are a few main ways we can go about testing their functionality. The first way would be to import our models directly into our test file, and then we can run ORM commands and see if the data was created properly. Another way is to call on view functions like we have already done, and test that those functions worked properly in creating, editing, or deleting our entities. Ultimately, we should have tests that cover both, because if a test fails in a view function but not in the direct access to models we can more easily narrow down the reason for failure.

First we are going to test to see if the models work by directly acccessing them from the tests we write. We need to import the models so we can access them, and we will write a test to create a single Album.

```python
from django.test import TestCase, Client

#Importing the models so we can access
from .models import *

class AlbumTest(TestCase):

# Adding this method below the other tests
    def test_model_creation(self):
        a = Album.objects.create(title = "Testing Time", artist = "The Pythons", year = 2018)
        self.assertEqual(a.title, "Testing Time")
        self.assertEqual(a.artist, "The Pythons")
        self.assertEqual(a.year, 2018)
```

What this will do is confirm that the album is created properly and that our database is set up correctly. Once we know this test works, it would be really nice to have a way to set up a dummy piece of data for us to use in all of our tests, rather than have to create a new Album for each individual test we have. Django's tests store this data temporarily in memory, and normally tests won't be able to access the DB data between one another, but there is a special function we can create that will allow us to have a more traditional system:

```python
class AlbumTest(TestCase):
    # This method will be called a single time before the tests begin, and can seed the DB for us

    @classmethod
    def setUpTestData(cls):
        Album.objects.create(title = "A Test Album", artist = "The Pythons", year = 2200)
```

This isn't a test itself, so we waited to implement the code until we knew that our models worked properly. But now we will have a single Album that is accessible to each individual test, which will help us more easily test the edit and delete functions.

We are going to use this to implement the rest of the model CRUD tests:

```python
    # Adding more tests to the class

    def test_model_get(self):
        a = Album.objects.get(id = 1)
        self.assertEqual(a.id, 1)
        self.assertIsInstance(a, Album)

    def test_model_edit(self):
        a = Album.objects.first()
        a.title = "EditedTitle"
        a.artist = "Django Doug"
        a.year = 1999
        a.save()
        edited_a = Album.objects.first()
        self.assertEqual(edited_a.title, "EditedTitle")
        self.assertEqual(edited_a.artist, "Django Doug")
        self.assertEqual(edited_a.year, 1999)

    def test_model_delete(self):
        # Delete will return a tuple that holds the number of entities deleted in the 0 index
        num_deleted = Album.objects.get(id = 1).delete()[0]
        self.assertEqual(num_deleted, 1)
```

The `test_model_get` simply retrieves an entity from the DB and ensures it is of the correct type and also ensures that the id matches what we queried.

The `test_model_edit` grabs an entity, updates it, and then asserts that all of the values were updated properly.

The `test_model_delete` tests to make sure that only one specific Album got deleted. The num_deleted would change if we ran a .filter(), .exclude(), or .all() instead of a .get(), so we might have to change the hardcoded '1' in the future if we were expecting more deletes.

We now have tests that make sure every CRUD operation works properly starting in our models. Coming up we will transition into testing to make sure that the views are interacting correctly with the models, and we will more closely replicate the typical actions a user will make on our site.

Head to Lesson 6 to see the final piece of the puzzle, testing our views' interaction with our models.
[Continue to Lesson_06](./lesson_06_django_views.md)