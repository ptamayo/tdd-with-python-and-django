# Unit Tests in Python

In order to design an application with a TDD approach we will start utilizing a specific type of test called a **unit test**. A unit test is a specific test we design to test a single part of a system, or a single unit. This could be something as simple as making sure that a function returns a + b, or something more complex like making sure a Django view function sent a correct context dictionary. All in all, we are designing a test around a single piece of our application.

## Thinking About Our First Test

For this exercise, we won't be following every letter of the TDD method just yet, as we are more focused on introducing unit testing for now.

For all of our work in this module, create a folder on your desktop called **python_tdd**. We will be keeping all of our work here for this module.

Now inside of the **python_tdd** folder create a file called first_test.py, and in it a single function called sum_two, which will add two numbers and return the total.

##### python_tdd/first_test.py
```python
def sum_two(a,b):  
    return a + b
```

Conceptually, it is easy to think of a few tests to see if this works. In pseudo:

```
sum_two given 5 and 7
returns 12
```

```
sum_two given -5 and 5
returns 0
```

When we implement the next few steps, we will be creating a unit test for this specific piece of code, the function sum_two.

## Coding Our First TestCase

The way Python's `unittest` module works is by defining a class that inherits from **TestCase**, which allows us to define methods that will run automatically when we run our test file. The TestCase class is designed to run any method whose name begins with **test**.

Let's add the proper import to the script we created a moment ago and we will start to build our test class.

##### python_tdd/first_test.py
```python
import unittest
# Notice that we are now importing unittest

def sum_two(a,b):
    return a + b


# Below this line we are creating our first class that will hold all of our tests
class MainTest(unittest.TestCase):

    # We will create individual tests as methods within this class, and the system knows to run any method whose name begins with 'test'
    def test_hello(self):
        print("Hello from the tests")
```

Now that we have implemented a single 'test' (not that the particular method is testing our function just yet), we can run this file and see if there is a result.

The command to run the tests that exist in this file:

```
python -m unittest first_test.py
```

What you will see in your terminal when you run the application is the result for all the test that have run:

```
Hello from the tests
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
```

In this case, there was no 'real' test, but we can see clearly that our code ran the method that we created. Try to add a new method to the class and name it test_sum_two, place inside of it just a print statement, and run the code again. If you don't see both of the print statements appear, double check your syntax and when it's working, continue.

What we have done so far is to create the general structure for our tests, our Test Case. Next we will be adding in the logic that actually validates whether the initial function in question works!

## Asserting a Result

Now that we have the structure of our test, it is time to start validating that are existing code works. To do this, there is a set of methods that the TestCase class provides, know as assert methods.


##### Assert Methods
|           Method          |      Checks That     | New In |
|:-------------------------:|:--------------------:|:------:|
| assertEqual(a,b)          |        a == b        |        |
| assertNotEqual(a,b)       |        a != b        |        |
| assertTrue(x)             |    bool(x) is True   |        |
| assertFalse(x)            |   bool(x) is False   |        |
| assertIs(a, b)            |        a is b        |   3.1  |
| assertIsNot(a, b)         |      a is not b      |   3.1  |
| assertIsNone(x)           |       x is None      |   3.1  |
| assertIsNotNone(x)        |     x is not None    |   3.1  |
| assertIn(a, b)            |        a in b        |   3.1  |
| assertNotIn(a, b)         |      a not in b      |   3.1  |
| assertIsInstance(a, b)    |   isinstance(a, b)   |   3.2  |
| assertNotIsInstance(a, b) | not isinstance(a, b) |   3.2  |

There are quite a few different options, and the usage of each depends on the scenario. Which do you think is most appropriate for the tests described by the pseudocode earlier?

assertEqual will work just fine in this scenario. Revisiting the code we had earlier:

```
sum_two given 5 and 7
returns 12
```

We can see that with assertEqual we really just need to determine if the returned value of our function is equal to a hardcoded value we provide. Let's implement this now:

```python

class MainTest(unittest.TestCase):

    def test_hello(self):
        print("Hello from the tests")

    ### If you didn't already create the below function, add it now ###
    def test_sum_two(self):
        returned_value = sum_two(5,7)
        
        # Here we call on self, and utilizing the methods we inherited from TestCase we can call on the assertEqual method, providing our inputs
        self.assertEqual(returned_value, 12)

```

After adding the logic to the `test_sum_two` method, go ahead and try to run the code again to see what happens. Your output might look something like this:

```
Hello from the tests
..
----------------------------------------------------------------------
Ran 2 tests in 0.000s

OK
```

We see that now 2 tests are being called and that they both have passed. Try to change the values we send into the function call `returned_value = sum_two(5,6)`. This will result in 11 being returned, let's see show this effects the test:

```
Hello from the tests
.F
======================================================================
FAIL: test_sum_two (first_test.MainTest)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "~/Desktop/python_tdd/first_test.py", line 51, in test_sum_two
    self.assertEqual(returned_value, 12)
AssertionError: 11 != 12

----------------------------------------------------------------------
Ran 2 tests in 0.001s

FAILED (failures=1)
```

... And we see a very different result: 2 tests were run, and we got one to fail. We clearly see that in the Traceback there was `AssertionError: 11 != 12`. The 11 returned from the function did not match the 12 that we were expecting.

While it may seem like a very simple test, the assertEqual method is one that we will be using quite often as we continue with this module. Let's go ahead and correct the line so we are back at `returned_value = sum_two(5,7)`, and we will also add one more line just under the existing assertEqual: 

```python
### Inside of the MainTest class we had created ###
    def test_sum_two(self):
        returned_value = sum_two(5,7)
        self.assertEqual(returned_value, 12)

        ### Adding ###
        self.assertEqual(sum_two(-5,5), 0)
```

When we run our code again, we will see that oddly enough the number of tests run doesn't change. This is due to the fact that Python only considers a test to be the individual test methods run, not the number of assert methods called. With this in mind, we can create one test that really makes multiple different asserts to test all possible inputs.

You can also see that we are able to just call the function `sum_two` from inside of the `assertEqual` call rather than store the returned value in a variable and place it there; this is more streamlined and considered better practice.

## Recap

To sum up the main points of how we can write a test and run it:

    1. `import unittest` and create a class that inherits from `unittest.TestCase`
    2. Define a method beginning with `test` so it will get run by TestCase
    3. Determine which assert method is most appropriate for the function we are testing
    4. Create a few different asserts in our test method so we can test different inputs in one go
    5. Run the tests with `python -m unittest first_test.py` and see the result

Get ready, because next you will be tasked with defining a few tests of your own!

Head into Exercise 1 and give it your best shot!
[Try Exercise_01](../exercises/exercise_01.md)